FROM python:3.9

COP app.py /app/app.py
COPY requirements.txt /app/requirements.txt

RUN pip install -r /app/requirements.txt

CMD ["python", "/app/app.py"]
