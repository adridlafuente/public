"""
This module contains a simple Flask application.
"""

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    """
    This function handles the root route '/' and returns a greeting.
    """
    return 'Hello from my Flask application in Docker!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)
